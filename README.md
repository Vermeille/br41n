# Brain

Propositional logic theorem prover.

Launch it, give it a set of true sentences, like

    A
    A ==> B

Then ask it a question

    B

and it will answer if it's true or false. Note that it handles only fully decidable questions.
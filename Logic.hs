#! /usr/bin/env runhugs +l
--
-- Logic.hs
-- Copyright (C) 2013 vermeille <guillaume.sanchez@epita.fr>
--
-- Distributed under terms of the MIT license.
--

module Logic where

data Logic  = Lit Char
            | Logic `Or` Logic
            | Logic `And` Logic
            | Logic `Impl` Logic
            | Logic `Equi` Logic
            | Paren Logic
            | Not Logic
            deriving (Eq, Ord)

instance Show Logic where
    show (Lit c) = [c]
    show (p `Or` q) = "(" ++ show p ++ " \\/ " ++ show q ++ ")"
    show (p `And` q) = "(" ++ show p ++ " /\\ " ++ show q ++ ")"
    show (p `Impl` q) = "(" ++ show p ++ " ==> " ++ show q ++ ")"
    show (p `Equi` q) = "(" ++ show p ++ " <=> " ++ show q ++ ")"
    show (Not p) = "!" ++ show p
    show (Paren p) = "(" ++ show p ++ ")"

(\/) = Or
(/\) = And
(==>) = Impl
(<=>) = Equi



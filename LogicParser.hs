#! /usr/bin/env runhugs +l
--
-- LogicParser.hs
-- Copyright (C) 2013 vermeille <guillaume.sanchez@epita.fr>
--
-- Distributed under terms of the MIT license.
--

module LogicParser (parse) where

import Logic

data Lexem  = TokOr
            | TokAnd
            | TokImpl
            | TokEq
            | TokNot
            | TokOpar
            | TokCpar
            | TokComa
            | TokVar Char
            deriving (Show, Eq)

lex :: String -> [Lexem]
lex [] = []
lex ('\n':cs)= []
lex (' '        :cs)    = LogicParser.lex cs
lex ('=':'=':'>':cs)    = TokImpl:LogicParser.lex cs
lex ('<':'=':'>':cs)    = TokEq:LogicParser.lex cs
lex ('/':'\\'   :cs)    = TokAnd:LogicParser.lex cs
lex ('\\':'/'   :cs)    = TokOr:LogicParser.lex cs
lex ('~'        :cs)    = TokNot:LogicParser.lex cs
lex (','        :cs)    = TokComa:LogicParser.lex cs
lex ('('        :cs)    = TokOpar:LogicParser.lex cs
lex (')'        :cs)    = TokCpar:LogicParser.lex cs
lex (c          :cs)    = TokVar c:LogicParser.lex cs

parse = fst . parseEq . LogicParser.lex

parseTemplate :: Lexem ->
                 ([Lexem] -> (Logic, [Lexem])) ->
                 (Logic -> Logic -> Logic) ->
                 [Lexem] ->
                 (Logic, [Lexem])

parseTemplate match pnext op ts =
    let (l, ts') = pnext ts
    in
        if [] /= ts' && head ts' == match then
            let res' = parseTemplate match pnext op (tail ts')
            in
                (l `op` (fst res'), snd res')
        else
            (l, ts')

parseEq   = parseTemplate TokEq parseImpl (<=>)
parseImpl = parseTemplate TokImpl parseOr (==>)
parseOr   = parseTemplate TokOr parseAnd (\/)
parseAnd  = parseTemplate TokAnd parseNot (/\)
parseNot (TokNot:ts) =
    let res = parseId ts
    in (Not $ fst res, snd res)
parseNot ts = parseId ts
parseId (TokVar c:ts) = (Lit c, ts)
parseId (TokOpar:ts) =
    case parseEq ts of
        (l, TokCpar:ts') -> (l, ts')
        _ -> error "no closing parent"


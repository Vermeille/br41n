#! /usr/bin/env runhugs +l
--
-- LogicSolver.hs
-- Copyright (C) 2013 vermeille <guillaume.sanchez@epita.fr>
--
-- Distributed under terms of the MIT license.
--

module LogicSolver (flatten, solve, resol) where

import Debug.Trace
import Logic
import Data.List

bicondElim :: Logic -> Logic
bicondElim (Lit c) = Lit c
bicondElim (Not p) = Not $ bicondElim p
bicondElim (Paren p) = Paren $ bicondElim p
bicondElim (p `Or` q) = (bicondElim p) \/ (bicondElim q)
bicondElim (p `And` q) = (bicondElim p) /\ (bicondElim q)
bicondElim (p `Impl` q) = bicondElim (Not p) \/ bicondElim q
bicondElim (p `Equi` q) = bicondElim (p ==> q) /\ bicondElim (q ==> p)

deMorgan :: Logic -> Logic
deMorgan (Lit c) = Lit c
deMorgan (Not (p `Or` q)) = (Not (deMorgan p)) /\ (Not (deMorgan q))
deMorgan (Not (p `And` q)) = (Not (deMorgan p)) \/ (Not (deMorgan q))
deMorgan (Not (Not p)) = deMorgan p
deMorgan (Not p) = Not $ deMorgan p
deMorgan (Paren p) = Paren $ deMorgan p
deMorgan (p `Impl` q) = (deMorgan p) ==> (deMorgan q)
deMorgan (p `Equi` q) = (deMorgan p) <=> (deMorgan q)
deMorgan (p `Or` q) = deMorgan p \/ deMorgan q
deMorgan (p `And` q) = deMorgan p /\ deMorgan q

distribute :: Logic -> Logic
distribute (Lit c) = Lit c
distribute (Not s) = Not (distribute s)
distribute (Paren s) = Paren (distribute s)
distribute (p `Equi` q) = distribute p <=> distribute q
distribute (p `Impl` q) = distribute p ==> distribute q
distribute (r `Or` (p `And` q)) =
    let r' = distribute r
    in (distribute p \/ r') /\ (distribute q \/ r')
distribute (p `Or` q) = distribute p \/ distribute q
distribute (p `And` q) = distribute p /\ distribute q

cnf :: Logic -> [[Logic]]
cnf (Lit c) = [[Lit c]]
cnf (Not (Not s)) = [[s]]
cnf (Not s) = [[Not s]]
cnf (p `Or` q) = [head (cnf p) ++ head (cnf q)]
cnf (p `And` q) = cnf p ++ cnf q

flatten :: Logic -> [[Logic]]
flatten = cnf . distribute . deMorgan . bicondElim

isSubsetOf xs ys = all (flip elem (fmap sort ys)) (fmap sort xs)

genPairs xs = [ (x, y) | x <- xs, y <- xs, x /= y]

solve :: [[Logic]] -> [[Logic]] -> Bool
solve clauses new =
    case inner ([ (x, y) | x <- clauses, y <- clauses, x /= y ]) new of
        (_, True) -> True
        (new', False) ->
            if new' == [] || new' `LogicSolver.isSubsetOf` clauses then
                False
            else
                solve (clauses ++ new') new'

inner :: [([Logic], [Logic])] -> [[Logic]] -> ([[Logic]], Bool)
inner [] new = (new, False)
inner (c:cs) new =
    case plResolve c of
        [] -> trace ("    => ([], True)") ([], True)
        resolvents ->
                if tautology resolvents || resolvents `elem` new then
                    inner cs new
                else
                    inner cs (resolvents : new)

negate :: Logic -> Logic
negate (Not p) = p
negate p = Not p

tautology [] = False
tautology (x:xs) = (LogicSolver.negate x) `elem` xs || tautology xs

plResolve :: ([Logic], [Logic]) -> [Logic]
plResolve ([], []) = []
plResolve ([], ys) = ys
plResolve (xs, []) = xs
plResolve (xs, ys) =
    let xs' = simplify xs
        ys' = simplify ys
    in
        sort $ resol $ union xs' ys'

resol (x:xs) =
    if LogicSolver.negate x `elem` xs then
        filter (/= (LogicSolver.negate x)) xs
    else
        x:resol xs
resol [] = []



simplify [] = []
simplify (p:ps) =
    if (LogicSolver.negate p) `elem` ps then
        simplify $ filter (/= (LogicSolver.negate p)) ps
    else
        p:filter (/= p) ps



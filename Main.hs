#! /usr/bin/env runhugs +l
--
-- Main.hs
-- Copyright (C) 2012 vermeille <guillaume.sanchez@epita.fr>
--
-- Distributed under terms of the MIT license.
--

module Main where

import Debug.Trace
import Control.Monad.State
import Control.Error
import Data.Maybe
import Data.Either
import LogicParser
import LogicSolver
import Logic

morganTest = Not $ (Lit 'p') \/ (Lit 'q')
equiTest = (Lit 'P' \/ (Lit 'Q' /\ Lit 'R')) -- (Lit 'p') <=> (Lit 'q')

getKB () = do
        sexpr <- getLine
        if sexpr == "" then
            return []
        else do
            next <- getKB ()
            return (parse sexpr:next)

flattenDB [] = []
flattenDB (p:ps) = flatten p ++ flattenDB ps

entails :: [Logic] -> Logic -> Bool
entails kb query = solve (flattenDB kb ++ flatten (Not query)) []

main = do
        putStrLn "\x1B[33mPlease type some expressions followed by<CR><CR>\x1B[0m"
        kb <- getKB ()
        putStrLn "\x1B[33mYour database:\x1B[0m"
        putStrLn $ show kb
        putStrLn "\x1B[33mEnter an expression:\x1B[0m"
        querystr <- getLine
        let query = parse querystr
        putStrLn $ show $ (flatten (Not query)) ++ flattenDB kb
        putStrLn $ show $ entails kb query
